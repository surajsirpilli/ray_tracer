///////////////////////////////////////////////////////////////////////
//
//  Ray Tracing Shell
//
///////////////////////////////////////////////////////////////////////
import java.util.*;
int screen_width = 300;
int screen_height = 300;

// global matrix values
PMatrix3D global_mat;
float[] gmat = new float[16];  // global matrix values
scene s;
sceneBuilder sb;
transformations mT;
int timer;

vec tDiffuse = new vec(0,0,0);
vec tAmbient = new vec(0,0,0);
float scale=1;
// Some initializations for the scene.

void setup() {
    size (screen_width, screen_height,P3D);  // use P3D environment so that matrix commands work properly
    noStroke();
    colorMode (RGB, 1.0);
    background (0, 0, 0);
   

    // grab the global matrix values (to use later when drawing pixels)
   // PMatrix3D global_mat = (PMatrix3D) getMatrix();
 //   global_mat.get(gmat);  
  //  resetMatrix();    // you may want to reset the matrix here
   // printMatrix();
    // s = new scene();
   // sb = new sceneBuilder(s);
    mT = new transformations();
    s = new scene();
    //interpreter("rect_test.cli");
    interpreter("t09.cli");
    sb = new sceneBuilder(s);
    sb.renderScene();
    
}

// Press key 1 to 9 and 0 to run different test cases.

void keyPressed() {
    switch(key) {
    case '1':  
        s = new scene();
        interpreter("t01.cli"); 
        sb = new sceneBuilder(s);
        sb.renderScene();
        break;
    case '2':  
        s = new scene();
        interpreter("t02.cli"); 
        sb = new sceneBuilder(s);
        sb.renderScene();
        break;
    case '3':  
        s = new scene();
        interpreter("t03.cli"); 
        sb = new sceneBuilder(s);
        sb.renderScene();
        break;
    case '4':  
        s = new scene();
        interpreter("t04.cli"); 
        sb = new sceneBuilder(s);
        sb.renderScene(); 
        break;
    case '5':  
        s = new scene();
        interpreter("t05.cli"); 
        sb = new sceneBuilder(s);
        sb.renderScene();
        break;
    case '6':  
        s = new scene();
        interpreter("t06.cli"); 
        sb = new sceneBuilder(s);
        sb.renderScene();
        break;
    case '7':  
        s = new scene();
        interpreter("t07.cli"); 
        sb = new sceneBuilder(s);
        sb.renderScene();
        break;
    case '8':  
        s = new scene();
        interpreter("t08.cli"); 
        sb = new sceneBuilder(s);
        sb.renderScene();
        break;
    case '9':  
        s = new scene();
        interpreter("t09.cli"); 
        sb = new sceneBuilder(s);
        println(s.objects.size());
        sb.renderScene();
        break;
    case '0':  
        s = new scene();
        interpreter("t10.cli"); 
        println(s.objects.size());
        sb = new sceneBuilder(s);
        sb.renderScene();
        break;
    case 'q':  
        exit(); 
        break;
    }
}

//  Parser core. It parses the CLI file and processes it based on each 
//  token. Only "color", "rect", and "write" tokens are implemented. 
//  You should start from here and add more functionalities for your
//  ray tracer.
//
//  Note: Function "splitToken()" is only available in processing 1.25 or higher.

void interpreter(String filename) {
    
    boolean pushFlag = false;
    String str[] = loadStrings(filename);
    if (str == null) println("Error! Failed to read the file.");
    
    for (int i=0; i<str.length; i++) {

        String[] token = splitTokens(str[i], " "); // Get a line and parse tokens.
        String[] prevToken = {""};
        String[] nextToken = {""};
        String[] t1 = {""}; String[] t2 = {""}; String[] t3 = {""}; String[] t4 = {""};
        
        if (i>1) 
        {
            prevToken = splitTokens(str[i-1], " ");
        }
        
        if (i < str.length-1)
        {
            nextToken = splitTokens(str[i+1], " ");
        } 
        
        if (i < str.length-1)
        {
            t1 = splitTokens(str[i+1], " ");
        }
        if (i < str.length-2)
        {
            t2 = splitTokens(str[i+2], " ");
        }
        if (i < str.length-3)
        {
            t3 = splitTokens(str[i+3], " ");
        }
        if (i < str.length-4)
        {
            t4 = splitTokens(str[i+4], " ");
        }
        
        
        if (token.length == 0) continue; // Skip blank line.

        if (token[0].equals("fov")) {
            s.cam.FOV = float(token[1]);
        }
        else if (token[0].equals("background")) {
            s.background = color(float(token[1]), float(token[2]), float(token[3]));
        }
        else if (token[0].equals("point_light")) {

            pt pos = new pt(float(token[1]), float(token[2]), float(token[3]));
            vec col = V3(float(token[4]), float(token[5]), float(token[6]));
            pointLight p = new pointLight(col, pos);
            s.lights.add(p);
        }
        else if (token[0].equals("diffuse")) {
           
                tDiffuse.setTo(float(token[1]), float(token[2]), float(token[3])); 
                tAmbient.setTo(float(token[4]), float(token[5]), float(token[6]));
            
        }    
        else if (token[0].equals("begin")) {
            // TODO
            triangle t = new triangle();        
            t.diffuse.setTo(tDiffuse);   
            t.ambient.setTo(tAmbient);
            if(!pushFlag)
            {
                if(t1.length > 0)
                t.v1 = new pt(float(t1[1]), float(t1[2]), float(t1[3]));
                if(t2.length > 0)
                t.v2 = new pt(float(t2[1]), float(t2[2]), float(t2[3]));
                if(t3.length > 0)
                t.v3 = new pt(float(t3[1]), float(t3[2]), float(t3[3]));
                
            }
            else
            {
                if(t1.length > 0)
                {
                    float[] v1 = {float(t1[1]),float(t1[2]), float(t1[3]),1};
                    float[] newV = {1,1,1,1};
                    mT.cMat.mult(v1,newV);
                    t.v1 = new pt(newV[0],newV[1],newV[2]);   
                }
                
                if(t2.length > 0)
                {
                    float[] v2 = {float(t2[1]),float(t2[2]), float(t2[3]),1};
                    float[] newV = {1,1,1,1};
                    mT.cMat.mult(v2,newV);
                    t.v2 = new pt(newV[0],newV[1],newV[2]);   
                }
                
                if(t3.length > 0)
                {
                    float[] v3 = {float(t3[1]),float(t3[2]), float(t3[3]),1};
                    float[] newV = {1,1,1,1} ;
                    mT.cMat.mult(v3,newV);
                    t.v3 = new pt(newV[0],newV[1],newV[2]);  
                }
            }
            
            s.objects.add(t);
        
        }
        else if (token[0].equals("end")) {
        
        }
        else if (token[0].equals("vertex")) {
            // TODO
            
        }
        else if (token[0].equals("sphere")) {
            float[] c = {float(token[2]), float(token[3]), float(token[4]),1};
            float[] newC = {1,1,1,1};
            if(pushFlag)
            {
               mT.cMat.mult(c,newC);
            }
            sphere sp;
            
            if(!pushFlag)
                sp = new sphere(float(token[1]),c[0],c[1],c[2]);
            else
                 sp = new sphere(float(token[1]),newC[0],newC[1],newC[2]);   
                 
             sp.diffuse.setTo(tDiffuse);   
             sp.ambient.setTo(tAmbient);
            
            s.objects.add(sp);
        }
        else if (token[0].equals("push")) {
                pushFlag = true;
                PMatrix3D mat = new PMatrix3D();
                mat.set(mT.cMat); 
                mT.st.push(mat);
            
        }
        else if (token[0].equals("pop")) {
            pushFlag = false;
            mT.cMat.set(mT.st.pop());
              
        }
        else if (token[0].equals("translate")) {
                mT.translate(float(token[1]),float(token[2]),float(token[3]));  
        }
        else if (token[0].equals("rotate")) {
         
                float angle = radians(float(token[1]));
                mT.rotate(angle,float(token[2]),float(token[3]), float(token[4]));       
                println("Rotate :"+float(token[1])+" "+float(token[2])+" "+float(token[3])+" "+float(token[4]));
        }
        else if (token[0].equals("scale")) {
           
                scale *= float(token[1]);
                mT.scale(float(token[1]),float(token[2]),float(token[3]));
        }
        else if (token[0].equals("read")) {  // reads input from another file
            interpreter (token[1]);
        }
        else if (token[0].equals("color")) {
            float r = float(token[1]);
            float g = float(token[2]);
            float b = float(token[3]);
            fill(r, g, b);
        }
        else if (token[0].equals("rect")) {
            float x0 = float(token[1]);
            float y0 = float(token[2]);
            float x1 = float(token[3]);
            float y1 = float(token[4]);
            rect(x0, screen_height-y1, x1-x0, y1-y0);
        }
        else if (token[0].equals("write")) {
            // save the current image to a .png file
            save(token[1]);
        }
        else if (token[0].equals()){
            
        }
        
    }
    
}

//  Draw frames.  Should be left empty.
void draw() {
}

// when mouse is pressed, print the cursor location
void mousePressed() {
    println ("mouse: " + mouseX + " " + mouseY);
}

