public class matrix3D
{
    float[][] matrix = new float[4][4];
    public matrix3D()
    {
        matrixIdentity();
    }

    void matrixReset()
    {
        for (int i=0; i<4; i++)
        {
            for (int j=0; j<4; j++)
                matrix[i][j] = 0;
        }
    }

    void matrixIdentity()
    {
        matrixReset();
        matrix[0][0] = matrix[1][1] = matrix[2][2] = matrix[3][3] = 1;
    }

    void matrixMult(matrix3D M)
    {
        matrix3D temp = new matrix3D();

        for (int i=0; i<4; i++)
        {
            for (int j=0; j<4; j++)
            {
                temp.matrix[i][j] = (matrix[i][0]*M.matrix[0][j]) + (matrix[i][1]*M.matrix[1][j]) + (matrix[i][2]*M.matrix[2][j]) + (matrix[i][3]*M.matrix[3][j]);
            }
        }
        for (int i=0; i<4; i++)
        {
            matrix[i][0] = temp.matrix[i][0];
            matrix[i][1] = temp.matrix[i][1];
            matrix[i][2] = temp.matrix[i][2];
            matrix[i][3] = temp.matrix[i][3];
        }
    }

    


    void matrixMult(matrix3D M1, matrix3D M2)
    {
        matrix3D temp = new matrix3D();

        for (int i=0; i<4; i++)
        {
            for (int j=0; j<4; j++)
            {
                temp.matrix[i][j] = (M1.matrix[i][0]*M2.matrix[0][j]) + (M1.matrix[i][1]*M2.matrix[1][j]) + (M1.matrix[i][2]*M2.matrix[2][j]) + (M1.matrix[i][3]*M2.matrix[3][j]);
            }
        }
        for (int i=0; i<4; i++)
        {
            M1.matrix[i][0] = temp.matrix[i][0];
            M1.matrix[i][1] = temp.matrix[i][1];
            M1.matrix[i][2] = temp.matrix[i][2];
            M1.matrix[i][3] = temp.matrix[i][3];
        }
    }
    
    
    void printMatrix()
    {
     for(int i=0; i<4; i++)
      {
       println("");
       for(int j=0; j<4; j++)
       {
            print(" "+matrix[i][j]+" ");       
       }
    }   
        
        
    }
    
    
}

