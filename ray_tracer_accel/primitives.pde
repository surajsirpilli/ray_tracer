public abstract class primitives
{
    public vec diffuse;
    public vec ambient;
    public primitives()
    {
    }   

    public abstract boolean intersect( ray R);
    public abstract vec checkLights(ray R, scene S, int k);
}

