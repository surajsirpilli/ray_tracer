public class sphere extends primitives
{
    float radius ;
    vec diffuse ;
    vec ambient;
    pt center = new pt(0, 0, 0);

    public sphere(float radius, float x, float y, float z)
    {
        this.radius = radius;
        center.setTo(x, y, z);
        ambient = new vec(0, 0, 0);
        diffuse = new vec(0, 0, 0);
    }

    public boolean intersect(ray R)
    {   
        vec d = R.direction;
        d.makeUnit();
        pt e = R.origin;
        pt c = center;
        float r = radius;
        vec eC = V3(c, e);
        float b = dot(d, eC);
        float b2 = b*b;
        float a = dot(d, d);

        float ac = (dot(d, d))*(dot(eC, eC)-r*r);
        if ((b2 - ac) < 0)       
        {
            return false;
        }
        else
        {    
            float t1 = (-b+sqrt(b2-ac))/a;
            float t2 = (-b-sqrt(b2-ac))/a;

            pt intersectP = null;  
            if (t1 > 0.1 && t2 > 0.1)
            {
                if (t1 < t2)
                {
                    intersectP = P(e.x+t1*d.x, e.y+t1*d.y, e.z+t1*d.z);
                    if(t1 < R.t)
                    {
                        R.point.setTo(intersectP);
                        R.t = t1;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    intersectP = P(e.x+t2*d.x, e.y+t2*d.y, e.z+t2*d.z);
                   // R.point.setTo(intersectP);
                   // return true;
                    if(t2 < R.t )       //if(intersectP.z > R.point.z)
                    {
                        R.point.setTo(intersectP);
                        R.t = t2;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (t1 < 0.1 && t2 > 0.1)
            {
                intersectP = P(e.x+t2*d.x, e.y+t2*d.y, e.z+t2*d.z);
                //R.point.setTo(intersectP);
                //return true;
                if(t2 < R.t)//if(intersectP.z > R.point.z)
                    {
                        R.point.setTo(intersectP);
                        R.t = t2;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }
            else if (t2 < 0.1 && t1 >0.1)
            {
                intersectP = P(e.x+t1*d.x, e.y+t1*d.y, e.y+t1*d.z);
                //R.point.setTo(intersectP);
                //return true;
                if(t1 < R.t)//if(intersectP.z > R.point.z)
                    {
                        R.point.setTo(intersectP);
                        R.t = t1;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }
            else 
            {
                return false;
            }   


            //return (t1 >= 0 || t2 >= 0);
        }
    }

    public vec checkLights(ray R, scene S, int current)
    {
        float x=ambient.x, y=ambient.y, z=ambient.z;

        vec d = R.direction;
        d.makeUnit();
        pt e = R.origin;
        pt c = center;
        float r = radius;
        vec eC = V3(c, e);
        float epsilon = 0.0;
        primitives rP ;
        pt intersectP = new pt(R.point);  

        for (int i=0; i<S.lights.size();i++)
        {
            boolean flagIntersect = false;
            vec normal = V3(center, intersectP);
            normal.makeUnit();
            pt orig = intersectP;
            vec dirE = V3(intersectP, S.lights.get(i).position);
            dirE.makeUnit();

            orig.x = orig.x + dirE.x*epsilon;
            orig.y = orig.y + dirE.y*epsilon;
            orig.z = orig.z + dirE.z*epsilon; 


            ray rL = new ray(orig, dirE);
            vec dir = rL.direction;
            dir.makeUnit();
            for (int j=0; j<S.objects.size();j++)
            {
                rP = S.objects.get(j);            
                {
                    if (rP.intersect(rL) == true )
                    {
                        flagIntersect = true ;
                    }
                }
            }
            if (flagIntersect == false)
            {
                float red = S.lights.get(i).lColor.x*diffuse.x*dot(normal, dir);
                float green = S.lights.get(i).lColor.y*diffuse.y*dot(normal, dir);
                float blue = S.lights.get(i).lColor.z*diffuse.z*dot(normal, dir);

                if (red < 0) red=0; 
                else if (red>1) red=1;
                x+=red;
                if (green < 0) green=0; 
                else if (green>1) green=1;
                y+=green;
                if (blue < 0) blue=0; 
                else if (blue>1) blue=1;
                z+=blue;
            }
        }

        vec colour = V3(x, y, z);
        return colour;
    }
}

