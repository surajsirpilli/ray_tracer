
public class transformations
{

    PMatrix3D rMat, cMat, tempMatrix;
    ArrayList<PMatrix3D> matrixStack = new ArrayList<PMatrix3D>();
    Stack<PMatrix3D> st = new Stack<PMatrix3D>();
    ArrayList<String> mStack = new ArrayList<String>();
    boolean pushFlag = true;

    public transformations()
    {
        rMat = new PMatrix3D();
        cMat = new PMatrix3D();
        tempMatrix = new PMatrix3D();
        matrixStack.add(cMat);
    }

    void translate(float x, float y, float z)
    {
        cMat.translate(x,y,z);
    }

    void scale(float x, float y, float z)
    {
        cMat.scale(x,y,z);
    }

    void rotate(float angle, float x, float y, float z)
    {   
        cMat.rotate(angle,x,y,z);   
    }

}

