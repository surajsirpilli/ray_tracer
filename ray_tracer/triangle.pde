public class triangle extends primitives
{
    vec diffuse;
    vec ambient;
    pt v1;
    pt v2;
    pt v3;
    pt p;

    public triangle()
    {
        ambient = new vec(0, 0, 0);
        diffuse = new vec(0, 0, 0);
        v1 = new pt();
        v2 = new pt();
        v3 = new pt();
        p = new pt(0,0,0);
    }
    public triangle(pt v1, pt v2, pt v3)
    {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        p = new pt(0, 0, 0);
        ambient = new vec(0, 0, 0);
        diffuse = new vec(0, 0, 0);
    }

    public boolean intersect(ray R)
    {
        pt e = R.origin ;
        vec d = R.direction;
        vec E1 = V3(v1,v2);
        E1.makeUnit();
        vec E2 = V3(v1,v3);
        E2.makeUnit();
        vec normal = cross(E1,E2);
        vec c = V3(e, v1);
        float t = dot(c, normal)/dot(d, normal);
        
        if (t < -0.00001)
            return false;
        //println("Value of t = " + t);
        pt intercept = new pt();
       
        intercept.x = e.x + t*d.x;
        intercept.y = e.y + t*d.y;
        intercept.z = e.z + t*d.z;
        
        p.setTo(intercept);
        if (checkInside(p) == true)
        {
            if( t < R.t)
            {
                R.t = t;
                R.point.setTo(intercept);
                return true;
            }
            else
            {
                 return false;   
            }
        }
        return false;
    }

    public vec checkLights(ray R, scene S, int k)
    {
        float epsilon = 0.1;
        float x = ambient.x, y = ambient.y, z = ambient.z;
        for (int i=0; i<S.lights.size();i++)
        {
            boolean flagIntersect = false;
            
            vec PA = V3(p,v1); 
            PA.makeUnit();
            vec PB = V3(p,v2); 
            PB.makeUnit();
      
            vec normal = cross(PA,PB);
            normal.makeUnit();
            
            pt orig = p;
            vec dirE = V3(p, S.lights.get(i).position);
            dirE.makeUnit();
            
            orig.x = orig.x + dirE.x*epsilon;
            orig.y = orig.y + dirE.y*epsilon;
            orig.z = orig.z + dirE.z*epsilon; 

            ray rL = new ray(orig, dirE);
            vec dir = rL.direction;
            dir.makeUnit();
            
            primitives rP;
            
            for (int j=0; j<S.objects.size();j++)
            {
                rP = S.objects.get(j);            
                {
                    if (rP.intersect(rL) == true )
                    {
                        flagIntersect = true ;
                    }
                }
            }
            
            if (flagIntersect == false)
            {
                float red = S.lights.get(i).lColor.x*diffuse.x*dot(normal, dir);
                float green = S.lights.get(i).lColor.y*diffuse.y*dot(normal, dir);
                float blue = S.lights.get(i).lColor.z*diffuse.z*dot(normal, dir);
                 

                if (red < 0) red=-red; 
                if (red>1) red=1;
                x+=red;
                if (green < 0) green=-green; 
                if (green>1) green=1;
                y+=green;
                if (blue < 0) blue=-blue; 
                if (blue>1) blue=1;
                z+=blue;
                
               
            }
        }

        vec colour = V3(x, y, z);
        return colour;
    }

    public boolean checkInside(pt P)
    {
        vec AB = V3(v1, v2);
        AB.makeUnit();
        vec BC = V3(v2, v3);
        BC.makeUnit();
        vec CA = V3(v3, v1);
        CA.makeUnit();


        vec AP = V3(v1, P); 
        AP.makeUnit();
        vec BP = V3(v2, P); 
        BP.makeUnit();
        vec CP = V3(v3, P); 
        CP.makeUnit();

        vec n1 = cross(AB, AP); 
        n1.makeUnit();
        vec n2 = cross(BC, BP); 
        n2.makeUnit();
        vec n3 = cross(CA, CP); 
        n3.makeUnit();

        if (dot(n1, n2)>=0 && dot(n2, n3)>=0 && dot(n3, n1)>=0)
        {
            return true;
        }
        else if (dot(n1,n2)<0 && dot(n2, n3)<0 && dot(n3, n1)<0)
        {
            return true;
        }
        else 
            return false;
    }
}

