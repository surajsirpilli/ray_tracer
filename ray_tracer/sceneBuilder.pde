public class sceneBuilder
{
    scene S;
    float rad;
    public sceneBuilder(scene S)
    {
        this.S = S;
    }   

    public void renderScene()
    {
        clearScene();
        boolean flag = false;
        rad = tan(radians(S.cam.FOV/2));
        // println(tan(radians(cam.FOV)));
        //println(" tanAngle is "+rad);
        int count = 0, ct=0;
        for (int i=0; i<300; i++)
        {
            for (int j=0; j<300; j++)
            {
                stroke(S.background);
                point(i, j);

                {
                    count++;
                    float x = convertX(i);
                    float y = convertY(j);
                    // println("x = "+x+"  y = "+y);    
                    vec dir = V3(S.eye, P(x, y, -1.0));
                    dir.makeUnit();
                    S.rays[i][j] = new ray(S.eye, dir);   
                    for (int k=0; k < S.objects.size() ; k++)
                    {   
                        primitives p = S.objects.get(k);
                        if (p.intersect(S.rays[i][j]) == true)
                        {
                            flag = true;

                            colorMode (RGB, 1.0);   
                            
                            vec cl = p.checkLights(S.rays[i][j], S, k);
                            color c;
                            //color c = color(1, 1, 1);
                            //stroke(c);
                            //point(i, j);

                            if (cl != null)
                            {   
                                ct++;
                                c = color(cl.x, cl.y, cl.z);
                                stroke(c);
                                point(i, j);
                            }
                        }
                    }
                }
            }
        }
    }
    
    void clearScene()
    {
        noStroke();
        colorMode (RGB, 1.0);
        background (0, 0, 0);        
    }

    public float convertX(float X)
    { 
        // println();
        // println("i is "+X);
        float newX = ((X-150)/150)*rad;       
        return newX;
    }

    public float convertY(float Y)
    {  
        // println("y is "+Y);
        float newY = ((150-Y)/150)*rad;   
        return newY;
    }
}

